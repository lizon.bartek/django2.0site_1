from django.contrib import admin
from .models import Game, ExtraInfo, Review, Bohater
# Register your models here.

@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    #fields = ('name', 'description', 'released')
    list_display = ('name', 'released','type','description')
    list_filter = ('type','released')
    search_fields = ('type', 'name')


admin.site.register(ExtraInfo)
admin.site.register(Review)
admin.site.register(Bohater)

from django.forms import ModelForm
from .models import Game


class GameForm(ModelForm):
    class Meta:
        model = Game
        fields = ['name', 'type', 'description', 'released','ign_rating', 'photo']
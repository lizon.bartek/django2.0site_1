from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from .models import Game, Review
from .forms import GameForm


from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from .serializers import UserSerializer, GameSerializer
# Create your views here.


def lista_gier(request):
    gry = Game.objects.filter(id__lt=250)
    return render(request, 'lista_gier.html', {'gry': gry})


@login_required
def nowa_gra(request):
    form = GameForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return redirect(lista_gier)

    return render(request, 'gra_forma.html', {'form': form})


@login_required
def edytuj_gre(request, id):
    gra = get_object_or_404(Game, pk=id)

    form = GameForm(request.POST or None, request.FILES or None, instance=gra)
    if form.is_valid():
        form.save()
        return redirect(lista_gier)

    return render(request, 'gra_forma.html', {'form': form})


def usun_gre(request, id):
    gra = get_object_or_404(Game, pk=id)

    if request.method == 'POST':
        gra.delete()
        return redirect(lista_gier)
    return render(request, 'potwierdz.html', {'gra': gra})


def opinie(request, id):

    gra = get_object_or_404(Game, pk=id)
    gry = Review.objects.filter(game=gra)



    return render(request, 'opinie.html', {'gra': gra, 'gry':gry, 'empty_stars':0})




class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GameViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Game.objects.all()
    serializer_class = GameSerializer

# Generated by Django 2.2.4 on 2019-08-14 06:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0019_auto_20190814_0851'),
    ]

    operations = [
        migrations.AlterField(
            model_name='extrainfo',
            name='Kampania',
            field=models.IntegerField(choices=[(1, 'Nie'), (0, 'Tak'), (2, 'Nie wiadomo')], default=0),
        ),
        migrations.AlterField(
            model_name='extrainfo',
            name='Multiplayer',
            field=models.IntegerField(choices=[(1, 'Nie'), (3, 'Tylko LAN'), (0, 'Tak'), (2, 'Nie wiadomo')], default=0),
        ),
        migrations.CreateModel(
            name='Bohater',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('imie', models.CharField(max_length=128)),
                ('nacja', models.CharField(max_length=128)),
                ('gry', models.ManyToManyField(to='main.Game')),
            ],
        ),
    ]

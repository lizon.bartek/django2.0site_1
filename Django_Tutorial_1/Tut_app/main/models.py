from django.db import models

# Create your models here.

class ExtraInfo(models.Model):

    MULTI = {
        (0, 'Tak'),
        (1, 'Nie'),
        (2, 'Nie wiadomo'),
        (3, 'Tylko LAN')
    }
    MULTI2 = {
        (0, 'Tak'),
        (1, 'Nie'),
        (2, 'Nie wiadomo')
    }

    Multiplayer = models.IntegerField(default=0, choices=MULTI)
    Kampania = models.IntegerField(default=0, choices=MULTI2)

class Game(models.Model):

    name = models.CharField(max_length=128)
    description = models.TextField(max_length=256, default='')
    type = models.CharField(max_length=60, default='')
    released = models.DateField(blank=True, null = True)
    ign_rating = models.DecimalField(decimal_places=2,max_digits=4,blank=True, null = True)
    photo = models.ImageField(blank=True, null = True, upload_to='obrazy')

    info = models.OneToOneField(
        ExtraInfo,
        on_delete=models.CASCADE,
        primary_key=False,
    )



    def __str__(self):
        return self.name_with_year()

    def name_with_year(self):
        return str(self.name) +' ('  + str(self.released) + ')'



class Review(models.Model):
    text = models.CharField(default='',blank=True, null=True, max_length=512)
    stars = models.IntegerField(default=5)
    game = models.ForeignKey(Game, related_name='reviews', on_delete=models.CASCADE)



    def __str__(self):
        return self.name()

    def name(self):
        return str(self.text[:50])


class Bohater(models.Model):
    imie = models.CharField(max_length=128)
    nacja = models.CharField(max_length=128)
    gry = models.ManyToManyField(Game)

    def __str__(self):
        return self.imie





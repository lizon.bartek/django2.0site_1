
from django.urls import path
from .views import *


from django.urls import include, path
from rest_framework import routers
from .views import UserViewSet,GameViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'games', GameViewSet)


urlpatterns = [
    path('gry/', lista_gier, name='lista_gier'),
    path('gry/nowa', nowa_gra, name='nowa_gra'),
    path('gry/edytuj/<int:id>/', edytuj_gre, name='edytuj_gre'),
    path('gry/usun/<int:id>/', usun_gre, name='usun_gre'),
    path('gry/opinie/<int:id>/', opinie, name='opinie'),
    path('', include(router.urls)),

]

from django.shortcuts import render
from .models import *

# Create your views here.


obj = BlogPost.objects.get(id=1)


def blog_post_detail_page(request):
    name = 'blog_post_detail.html'
    obj = BlogPost.objects.get(id=2)
    context = {'object': obj}
    return render(request, name, context)

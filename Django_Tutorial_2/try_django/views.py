from django.http import HttpResponse
from django.shortcuts import render


def home_page(request):
    title =  'Hello there...'
    list = [1, 2, 3, 4, 5]

    context = {"title": title, "list": [3,4,5]}
    if request.user.is_authenticated:
        context = {"title":title, 'list': list}
    return render(request, 'home.html', context)

def about_page(request):
    return render(request, 'about.html', {"title": "About"})
def contact_page(request):
    return render(request, 'contact.html', {"title": "Contact"})